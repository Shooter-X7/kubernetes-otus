# Знакомство с kubernetes
- master-node
    - управляющие приложения
        - kube-apiserver
            - API-шлюз - точка входа для всех взаимодействий компонентов kubernetes
            - предоставляет REST-сервис (работа над состояниями объектов)
            - Проверяет и конфигурирует API- объекты (pods,servoces и тд)
            - сохраняет состояние в etcd
        - etcd
            - база данных key-value, для хранения в памяти текущегго состояния
            - можно подпиасться на изменение како-го нибудь ключа
        - kube-controller-manager
            - отслеживает через API изменение состояни объектов
            - менеджер, который управляет контроллерами
                - replicaset controller
                - endpoints controller
                - namespace controller и т.д.
        - kube-sheduler
            - учет имеющихся ресурсов
            - учет требований к ресурсам
            - учет ограничений по ресурсам(affinity, anti-affinity)
            - решение где запустить контейнеры на основе ораничений и требований
- worker-node
    - наши приложения, в docker, containerd
    - kube-proxy
        - проксирует TCP и UDP (не HTTP)
        - используется для работы с сервисами
        - обеспечивает внутреннюю балансировку
    - kubelet
        - управляет контейнерами, запускает поды
        - общается с API-Server
    - container runtime

## конфигурация kubectl - описывается в конфиге
- context - куда подключаться
    - cluster - API сервера
    - user - пользователь для подключения к кластеру
    - namespase - область видимости - необязательно
    - name - имя контекста для идентификации

- cluster - API сервера
    - server - адрес kubernetes API - сервера
    - certificate-authority - корневой сертификат, которым подписан SSL-сертификат API- сервера
    - name - имя для идентификации

- pod
    - группа контейнеров (один или несколько)
    - минимальная сущность, управляемая kubernetes
    - у всех контейнеров общаие network,IPC,UTC,PID namespaces

- namespaces
    - default - для объектов у которых явно не определена принадлежность к другому нс
    - kube-system - для системных объектов kubernetes
    - kube-public - для объектов к которым нужен доступ из любой точки кластера
## разное
- yaml манифест
    - 1 файл может описывать много сущностей
    - обязательные поля:
        - apiVersion: $group_name/$version (v1alpha1, v1beta1, v1) грубо говоря это ссылка на версию API
        - kind: тип сущности
        - metadata: name, labels(метки), namespase, annotation

## init контейнеры
- Похожие на обычные контейнеры внутри pod, но есть отличия:
    - Запускаются до старта остальных контейнеров(app) в pod
    - Блокируют запуск app контейнеров
- используются для:
    - Проверки внешних зависимостей необходимых для старта app контейнеров
    - Подготовки данных для app контейнеров (например git clone)
    - Использования данных, которые нежелательно хранить в app контейнере


## команды
- kubectl cluster-info dump | less посмотреть информацию о кластере
- kubectl api-resources - посмотреть все сущности
- kubectl get componentstatuses (get cs) - вывести состояние системных компонентов
- kubectl run frontend --image nepetrov7/frontend --restart=Never --dry-run=client -o yaml > frontend-pod.yaml (--dry-run=client -o yaml - вывести без создания манифест создания пода в ямл)

## утилиты
- kubens - для переключения между неймспейсами
- kubectx - для переключения между кластерами

## подключить дашборд к кластеру
- kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.2.0/aio/deploy/recommended.yaml
- kubectl proxy
- http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

## полезные ссылки:
- https://kube-forwarder.pixelpoint.io/
- https://k9scli.io/
