- vault статусы
    - initialized - инициализировано ли хранилище
    - sealed - запечатано хранилище
    - treshhold - количество ключей для распечатки vault
    - unseal progress - соотношение количество введенных ключей к необходимым
- vault команды
    - при установке делаем `operator init`
        - выдаст набор unseal включей
        - выдаст root token
            - с помощью которого лучше создать админский токен
            - root токен отложить, а пользоваться админским
    - `operator unseal $KEY` - распечатать
    - `login $VAULT_TOKEN`
    - `secrets enable -path=mysecrets kv` - создать key value хранилище
    - `secrets list`
    - `kv put mysecrets/app user=igor` - положить key value - по этому пути
    - `kv get mysecret/app` - получить key value
- можно запустить vault, который будет заниматься auto unseal-ом
- policy
    - создаем файл
        ```
        cat app-policy.hcl
        path "mysecret/app"{
            capabilities = ["create", "read", "update"]
        }
        ```
    - применяем `vault policy write app-policy app-policy.hcl`
    - на вкладке policies в UI мы можем их увидеть
    - создаем токен `vault token create -field token -policy=app-policy`
- как ходить curl-ом
    - `curl --silent --header "X-Vault-Token: $VAULT_TOKEN" --request GET $URL_VAULT/v1/mysecret/app | jq ".data"`
- можно настроить так чтобы vault доверял кластеру kubernetes и кластер сам там регистрировался и получал необходимые токены
    - создаем serviceAccount `vault-auth` и даем ему clusterRole `system:auth-delegator`
    - в vault логинимся и включаем движок авторизации kubernetes `vault auth enable kubernetes` (либо в UI)
    - `export VAULT_SA_NAME=$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")`
    - `export SA_JWT_TOKEN=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 -d )`
    - `export SA_CA_CRT=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data.['ca\.crt']}" | base64 -d )`
    - `vault write auth/kubernetes/config token_reviewer_jwt="$SA_JWT_TOKEN" kubernetes_host="$KUBERNETES_HOST_URL_AND_PORT" kubernetes_ca_cert="$SA_CA_CRT" disable_iss_validation=true`
        - disable_iss_validation=true нуджен для кластера выше v1.21 тикет: `https://github.com/external-secrets/kubernetes-external-secrets/issues/721`
    - связываем serviceAccount kubernetes с policy в vault `vault write auth/kubernetes/role/example bound_service_account_names=vault-auth bound_service_account_namespaces=default policies=app-policy ttl=24h` дословно - "если что-то запущено в нейспейсе default с сервисным аккаунтом vault-auth, то это что-то получит права, которые прописаны в app-policy"
    - запускаем под в под этим сервисным аккаунтом
    - `export KUBE_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)` 
    - `curl --request POST --data '{"jwt": "'"$KUBE_TOKEN"'", "role": "example"}' $URL_VAULT/v1/auth/kubernetes/login | jq "."`
    - мы получим в ответе json, нас интересует поле `auth.client_token`
    - берем этот токен, помещаем в переменную `$VAULT_TOKEN`
    - обращаемся с этим токеном к vault `curl --silent --header "X-Vault-Token: $VAULT_TOKEN" --request GET $URL_VAULT/v1/mysecret/app | jq ".data"` и получаем key-value значение
- можно сделать проще - сделать init container, который будет запускаться перед запуском контейнера с приложением, получить данные из vault, по вышеописанной схеме и сложить в файл, который будет примонтирован к контейнеру с приложением
- и еще проще решение: установка vault-injector
    - `helm repo add hashicorp https://helm/releases/hashicorp.com`
    - `helm upgrade --install vault hashicorp/vault --set "injector.externalVaultAddr=http://URL_VAULT" --set "injector.authPath=auth/kubernetes"` - vault server ставить не надо, а injector надо.
    - vault-agent-injector будет при деплое со специальной аннотацией, добавлять туда sidecar контейнер, который и будет собирать данные с vault и складывать даныые из vault в конфиг в контейнере


```
apiVersion: v1
kind: ConfigMap
metadata:
  name: example-vault-agent-config
  namespace: default
data:
  vault-agent-config.hcl: |
    # Comment this out if running as sidecar instead of initContainer
    exit_after_auth = true

    pid_file = "/home/vault/pidfile"

    auto_auth {
        method "kubernetes" {
            mount_path = "auth/kubernetes"
            config = {
                role = "example"
            }
        }

        sink "file" {
            config = {
                path = "/home/vault/.vault-token"
            }
        }
    }

    template {
    destination = "/etc/secrets/index.html"
    contents = <<EOT
    <html>
    <body>
    <p>Some secrets:</p>
    {{- with secret "mysecrets/app" }}
    <ul>
    <li><pre>username: {{ .Data.user }}</pre></li>
    </ul>
    {{ end }}
    </body>
    </html>
    EOT
    }
---
apiVersion: v1
kind: Pod
metadata:
  name: vault-agent-example
  namespace: default
spec:
  serviceAccountName: vault-auth

  volumes:
  - configMap:
      items:
      - key: vault-agent-config.hcl
        path: vault-agent-config.hcl
      name: example-vault-agent-config
    name: config
  - emptyDir: {}
    name: shared-data

  initContainers:
  - args:
    - agent
    - -config=/etc/vault/vault-agent-config.hcl
    - -log-level=debug
    env:
    - name: VAULT_ADDR
      value: http://192.168.33.16:8200
    image: vault
    name: vault-agent
    volumeMounts:
    - mountPath: /etc/vault
      name: config
    - mountPath: /etc/secrets
      name: shared-data

  containers:
  - image: nginx
    name: nginx-container
    ports:
    - containerPort: 80
    volumeMounts:
    - mountPath: /usr/share/nginx/html
      name: shared-data
---
apiVersion: v1
kind: Pod
metadata:
  name: vault-agent-example
  namespace: default
  annotations:
    vault.hashicorp.com/agent-inject: "true"
    vault.hashicorp.com/agent-inject-secret-index.html: "mysecrets/app"
    vault.hashicorp.com/agent-inject-template-index.html: |
      <html>
      <body>
      <p>Some secrets:</p>
      {{- with secret "mysecrets/app" }}
      <ul>
      <li><pre>username: {{ .Data.user }}</pre></li>
      </ul>
      {{ end }}
      </body>
      </html>
    vault.hashicorp.com/role: "example"
spec:
  serviceAccountName: vault-auth

  containers:
  - image: nginx
    name: nginx-container
    ports:
    - containerPort: 80
    volumeMounts:
    - mountPath: /usr/share/nginx/html
      name: vault-secrets
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: role-tokenreview-binding
  namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
- kind: ServiceAccount
  name: vault-auth
  namespace: default
---
spec:
template:
  metadata:
    annotations:
      vault.hashicorp.com/agent-inject: "true"
      vault.hashicorp.com/role: "internal-app"
      vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data"
---
spec:
template:
  metadata:
    annotations:
      vault.hashicorp.com/agent-inject: "true"
      vault.hashicorp.com/agent-configmap: vault-injector-configmap

```