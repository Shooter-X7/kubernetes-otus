# kubernetes_templating
- Chart - пакет
    Chart.yml - описание пакета
- Values - параметры для пакета
- Release - сущность, которая возникает после установки пакета
    - Chart + Values = Release
    - 1 Upgrade = 1 Release

## helm
- `helm create` - для создания структуры чарта
- `helm template $path_to_chart` - проверить какие получились манифесты
- в основе helm лежит шаблонизатор Go документация:`https://pkg.go.dev/text/template`
- советы по написанию helm charts `https://helm.sh/docs/chart_best_practices/`
- annotations для hook прописываются так: `"helm.sh/hook": "pre-install"`
    - есть вес хуков `"helm.sh/hook-weight": "10"` - чем меньше вес - тем он выполняется раньше
    - после выполнения hook - часто нужно удалять артефакты после него, для этого есть annotationn `"helm.sh/hook-delete-policy"`

### helm 2
- `helm {install, upgrade, upgrade --install} $chart_name --name=$release_name --namespace=$namespase`
- для работы helm 2 нужно усатановить в кластер tiller, он принимает запросы от helm и взаимодействует с kube-api, хранит инфу о релизах в `configmaps`
    - установка: `helm init`
    - проверка: `helm version`

### helm 3
- убран tiller
- release теперь хранятся в нейспейсах
- зависимости указываются в Chart.yml
- экспериментальная поддежка OCI - совместимых registry для хранения Helm Charts
- LUA как альтернативный яхык описания скриптов

### Helm Secrets
- плагин для helm `https://github.com/zendesk/helm-secrets`
- Механизм удобного* хранения и деплоя секретов для тех, у кого нет HashiCorp Vault
- Возможность сохранить структуру зашифрованного файла (YAML, JSON)
- нужно установить helmfile: `https://github.com/roboll/helmfile`
    - полезный обзор: `https://itnext.io/setup-your-kubernetes-cluster-with-helmfile-809828bc0a9f`
    - так же будет доступен helm-diff

### kustomize
- `kubectl kustomize $overlays_dir_with_vars`
    - в этих файлах должны быть описания того какие значениями заменяться, `kind: Kustomization`
